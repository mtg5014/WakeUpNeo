package com.philogistics.wakeupneo;

/**
 * Created by mtg5014 on 12/27/16.
 * global constants
 */

public class Constants {
    public static final long DAY_IN_MILLIS = 86400000;
    public static final long HOUR_IN_MILLIS = 3600000;
    public static final long MINUTE_IN_MILLIS = 60000;
    public static final String TIME_IN_MILLIS = "timeInMillis";
    public static final int DEFAULT_SNOOZE_LENGTH = 10;
}
