package com.philogistics.wakeupneo.database.manager;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by mtg5014 on 12/27/16.
 * interface wrapper for a SugarRecord bean - to expose the parts of SugarRecord we want to use
 */

public interface SugarRecordManager<T extends SugarRecord>
{
    public long save(T sugarRecord);
    public boolean delete(T sugarRecord);
    public T findById(Class<T> clazz, long id);
    public List<T> find(Class<T> clazz, String whereClause, String whereArgs);
    public List<T> find(Class<T> clazz, String whereClause, String[] whereArgs, String groupBy, String orderBy, String limit);
    public List<T> findAll(Class<T> clazz);
}
