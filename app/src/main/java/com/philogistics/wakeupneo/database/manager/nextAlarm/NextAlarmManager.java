package com.philogistics.wakeupneo.database.manager.nextAlarm;

import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.SugarRecordManager;

import java.util.List;

/**
 * Created by mtg5014 on 12/27/16.
 * the manager for alarms - this manager uses a utilities class to perform the
 * android OS operations that need to be tightly coupled with alarm crud operations
 */

public interface NextAlarmManager extends SugarRecordManager<NextAlarm>
{
    /**
     * finds all alarms that are on
     * @param active
     * @return
     */
    public List<NextAlarm> findByActive(boolean active);

    /**
     * finds based on the time in milliseconds - which in our implementation should
     * also represent a single, distinct alarm
     * @param timeInMillis
     * @return
     */
    public NextAlarm findByTimeInMillis(long timeInMillis);

    /**
     * cautionary method to see if we have multiple alarms at the same time, so we can remove
     * any extras
     * @param timeInMillis
     * @return
     */
    public List<NextAlarm> findAllByTimeInMillis(long timeInMillis);

    /**
     * finds the next alarm that will sound
     * @return
     */
    public NextAlarm findNextActive();

    /**
     * skips an alarm forward or backward in time
     * @param nextAlarm
     * @param forward
     * @param millis
     */
    public void skip(NextAlarm nextAlarm, boolean forward, long millis);

    /**
     * dismisses the alarm (and skips it forward)
     * @param nextAlarm
     */
    public void dismiss(NextAlarm nextAlarm);

    /**
     * snoozes the alarm
     * @param nextAlarm
     */
    public void snooze(NextAlarm nextAlarm);

    /**
     * checks to see if an alarm is already sounding when we try to set off another alarm
     * @param nextAlarm
     * @return
     */
    public boolean collisionExists(NextAlarm nextAlarm);

    /**
     * if we are about to collide - defers the sounding of the colliding alarm
     * @param nextAlarm
     * @param deferTime
     */
    public void deferCollission(NextAlarm nextAlarm, long deferTime);
}
