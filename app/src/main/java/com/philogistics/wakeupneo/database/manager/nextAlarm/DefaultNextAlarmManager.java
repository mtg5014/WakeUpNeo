package com.philogistics.wakeupneo.database.manager.nextAlarm;

import android.content.Context;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.AbstractSugarRecordManager;
import com.philogistics.wakeupneo.database.manager.SugarRecordManager;

import java.util.List;

/**
 * Created by mtg5014 on 12/27/16.
 * implementation of NextAlarmManager
 */

public class DefaultNextAlarmManager extends AbstractSugarRecordManager<NextAlarm>
        implements NextAlarmManager, SugarRecordManager<NextAlarm>
{
    private NextAlarmUtilities nextAlarmUtilities;

    public DefaultNextAlarmManager(Context context)
    {
        nextAlarmUtilities = new NextAlarmUtilities(context);
    }

    @Override
    public long save(NextAlarm nextAlarm) {

        //in case we are changing the time - dismiss the old alarm
        NextAlarm dbAlarm = null;
        if(nextAlarm.getId() != null && nextAlarm.getId() > 0)
        {
            dbAlarm = findById(NextAlarm.class, nextAlarm.getId());
            nextAlarmUtilities.dismiss(dbAlarm,true);
        }

        //if we are changing the time or creating, dismiss whatever we are changing to, since we will recreate
        if(dbAlarm == null || dbAlarm.getTimeInMillis() != nextAlarm.getTimeInMillis())
        {
            nextAlarmUtilities.dismiss(nextAlarm,true);
        }

        //make sure we can't doCreate in the past
        while(nextAlarm.getTimeInMillis() < System.currentTimeMillis())
        {
            nextAlarm.setTimeInMillis(nextAlarm.getTimeInMillis() + Constants.DAY_IN_MILLIS);
        }

        //check for collisions - delete if found - this must happen before setting alarm to avoid cancelling
        //what we just set!
        if(!findAllByTimeInMillis(nextAlarm.getTimeInMillis()).isEmpty())
        {
            for(NextAlarm oldAlarm : findAllByTimeInMillis(nextAlarm.getTimeInMillis()))
            {
                delete(oldAlarm);
            }
        }

        if(nextAlarm.isActive())
        {
            nextAlarmUtilities.setAlarm(nextAlarm.getTimeInMillis(), nextAlarm.getTimeInMillis());
        }

        return super.save(nextAlarm);
    }

    @Override
    public void skip(NextAlarm nextAlarm, boolean forward, long millis)
    {
        if(forward)
        {
            nextAlarm.setTimeInMillis(nextAlarm.getTimeInMillis() + millis);
            save(nextAlarm);
        }
        else if(nextAlarm.getTimeInMillis() - millis > System.currentTimeMillis())
        {
            nextAlarm.setTimeInMillis(nextAlarm.getTimeInMillis() - millis);
            save(nextAlarm);
        }
        else
        {
            nextAlarmUtilities.cantSkipToast();
        }
    }

    @Override
    public boolean delete(NextAlarm nextAlarm)
    {
        nextAlarmUtilities.dismiss(nextAlarm,true);
        if(nextAlarm.getId() != null) {
            return super.delete(nextAlarm);
        }
        else
        {
            return false;
        }
    }

    @Override
    public void dismiss(NextAlarm nextAlarm)
    {
        nextAlarmUtilities.dismiss(nextAlarm,true);
        skip(nextAlarm,true, Constants.DAY_IN_MILLIS);
    }

    @Override
    public void snooze(NextAlarm nextAlarm)
    {
        nextAlarmUtilities.snooze(nextAlarm);
    }

    @Override
    public List<NextAlarm> findByActive(boolean active)
    {
        return NextAlarm.find(
                NextAlarm.class,
                "active = ?",
                Integer.valueOf(active ? 1 : 0).toString());
    }

    @Override
    public void deferCollission(NextAlarm nextAlarm, long deferTime) {
        nextAlarmUtilities.setAlarm(nextAlarm.getTimeInMillis(),System.currentTimeMillis() + deferTime);
    }

    @Override
    public NextAlarm findByTimeInMillis(long timeInMillis) {
        List<NextAlarm> all = findAllByTimeInMillis(timeInMillis);
        return all.isEmpty() ? null : all.get(0);
    }

    @Override
    public List<NextAlarm> findAllByTimeInMillis(long timeInMillis) {
        return find(
                NextAlarm.class,
                "time_in_millis = ?",
                Long.valueOf(timeInMillis).toString());
    }

    @Override
    public NextAlarm findNextActive()
    {
        List<NextAlarm> alarms =  find(
                NextAlarm.class,
                "active = ? AND time_in_millis > ?",
                new String[]{Integer.valueOf(1).toString(),
                        Long.valueOf(System.currentTimeMillis()).toString()},
                null,
                "time_in_millis",
                "1"
        );
        return alarms.isEmpty() ? null : alarms.get(0);
    }

    @Override
    public List<NextAlarm> findAll(Class<NextAlarm> clazz) {
        return find(
                clazz,
                null,
                null,
                null,
                "time_in_millis",
                null
        );
    }

    @Override
    public boolean collisionExists(NextAlarm nextAlarm) {
        List<NextAlarm> alarms = find(
                NextAlarm.class,
                "ringing = ? AND time_in_millis != ?",
                new String[]{Integer.valueOf(1).toString(),
                        Long.valueOf(nextAlarm.getTimeInMillis()).toString()},
                null,
                null,
                null
        );
        return alarms == null ? false : alarms.isEmpty() ? false : true;
    }
}
