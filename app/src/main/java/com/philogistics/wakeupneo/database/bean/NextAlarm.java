package com.philogistics.wakeupneo.database.bean;

import com.orm.SugarRecord;
import com.orm.dsl.NotNull;

import java.util.List;

/**
 * Created by mtg5014 on 5/18/2016.
 * the database bean representing an alarm
 */
public class NextAlarm extends SugarRecord{
    @NotNull
    private long timeInMillis;//the htime of the alarm in milliseconds
    @NotNull
    private boolean active;//on or off
    @NotNull
    private String ringtone;
    @NotNull
    private int snoozeOffset;//how many minutes to snooze
    @NotNull
    private boolean ringing;//hidden field representing an alarm that is currently ringing

    ///////////////////////////////////////
    ////////////Constructors
    ///////////////////////////////////////
    public NextAlarm(){}

    public NextAlarm(int timeInMillis, boolean active, String ringtone, int snoozeOffset, boolean ringing)
    {
        this.timeInMillis = timeInMillis;
        this.active = active;
        this.ringtone = ringtone;
        this.snoozeOffset = snoozeOffset;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    public List<NextAlarm> findActive(Boolean active)
    {
        return NextAlarm.find(NextAlarm.class,"active = ?", active.toString());
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getRingtone() {
        return ringtone;
    }

    public void setRingtone(String ringtone)
    {
        this.ringtone = ringtone;
    }

    public int getSnoozeOffset() {
        return snoozeOffset;
    }

    public void setSnoozeOffset(int snoozeOffset) {
        this.snoozeOffset = snoozeOffset;
    }

    public boolean isRinging() {
        return ringing;
    }

    public void setRinging(boolean ringing) {
        this.ringing = ringing;
    }
}
