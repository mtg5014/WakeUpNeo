package com.philogistics.wakeupneo.database.manager;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by mtg5014 on 12/27/16.
 * common sugarrecord operations
 */

public abstract class AbstractSugarRecordManager<T extends SugarRecord> implements SugarRecordManager<T>
{
    @Override
    public long save(T sugarRecord)
    {
        return T.save(sugarRecord);
    }

    @Override
    public boolean delete(T sugarRecord)
    {
        return T.delete(sugarRecord);
    }

    @Override
    public T findById(Class<T> clazz, long id) {
        return T.findById(clazz,id);
    }

    @Override
    public List<T> find(Class<T> clazz, String whereClause, String whereArgs) {
        return T.find(clazz,whereClause,whereArgs);
    }

    @Override
    public List<T> find(Class<T> clazz, String whereClause, String[] whereArgs, String groupBy, String orderBy, String limit) {
        return T.find(clazz,whereClause,whereArgs,groupBy,orderBy,limit);
    }

    @Override
    public List<T> findAll(Class<T> clazz) {
        return T.listAll(clazz);
    }
}
