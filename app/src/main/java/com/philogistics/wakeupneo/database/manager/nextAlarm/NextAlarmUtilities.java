package com.philogistics.wakeupneo.database.manager.nextAlarm;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;
import com.philogistics.wakeupneo.listener.service.MediaService;
import com.philogistics.wakeupneo.gui.activity.MainActivity;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.listener.receiver.AlarmReceiver;
import com.philogistics.wakeupneo.gui.notification.NotificationFactory;

/**
 * Created by mtg5014 on 12/27/16.
 * class that handles all common operations that need a context object
 * e.g. setting alarms, notifications, etc. in the android system so that this is
 * done consistently both by CRUD operations and by snoozing/dismissing an alarm from
 * various places
 */

public class NextAlarmUtilities
{
    private Context context;
    private AlarmManager alarmManager;

    protected NextAlarmUtilities(Context context)
    {
        this.context = context.getApplicationContext();
        alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
    }

    protected void setAlarm(long timeInMillisID, long timeInMillis)
    {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(Constants.TIME_IN_MILLIS, timeInMillisID);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                NextAlarmStaticUtilities.getUniqueIntForMillis(timeInMillisID), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //always cancelAlarm first - to prevent duplicates
        alarmManager.cancel(pendingIntent);

        Intent activityIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingActivityIntent = PendingIntent.getBroadcast(context, NextAlarmStaticUtilities.getUniqueIntForMillis(timeInMillisID)
                , activityIntent, 0);
        android.app.AlarmManager.AlarmClockInfo alarmClockInfo = new android.app.AlarmManager.AlarmClockInfo(timeInMillis, pendingActivityIntent);

        alarmManager.setAlarmClock(alarmClockInfo,
                pendingIntent);
    }

    protected void dismiss(NextAlarm nextAlarm, boolean cancelNotification)
    {
        cancelAlarm(nextAlarm.getTimeInMillis());

        //in this VERY speciall case, we use NextAlarm direclty to save the fact that the alarm is NOT ringing
        //the ringing field is systematic only and we want none of the side effects from he manager save method,
        //only to save the bean
        nextAlarm.setRinging(false);
        NextAlarm.save(nextAlarm);

        Intent mediaIntent = new Intent(context.getApplicationContext(), MediaService.class);
        context.getApplicationContext().stopService(mediaIntent);

        Intent alarmIntent = new Intent("finish_activity");
        context.sendBroadcast(alarmIntent);

        if(cancelNotification) {
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NextAlarmStaticUtilities.getUniqueIntForMillis(nextAlarm.getTimeInMillis()));
        }
    }

    protected void snooze(NextAlarm nextAlarm)
    {
        dismiss(nextAlarm,false);
        if(nextAlarm != null) {
            int snoozeInterval = nextAlarm.getSnoozeOffset() > 0 ? nextAlarm.getSnoozeOffset() : 10;
            setAlarm(nextAlarm.getTimeInMillis(), System.currentTimeMillis() + (Constants.MINUTE_IN_MILLIS * snoozeInterval));
        }
        else
        {
            Log.e("NextAlarm","No nextAlarm found in database on snoozeAndroidAlarmClock!");
        }

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder snoozeNotificationBuilder = NotificationFactory.NextAlarmSnoozeNotification(context.getApplicationContext(),context.getPackageName(), nextAlarm);
        notificationManager.notify(NextAlarmStaticUtilities.getUniqueIntForMillis(nextAlarm.getTimeInMillis()),snoozeNotificationBuilder.build());
    }

    private void cancelAlarm(long timeInMillisID)
    {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                NextAlarmStaticUtilities.getUniqueIntForMillis(timeInMillisID), intent, 0);

        alarmManager.cancel(pendingIntent);
    }

    protected void cantSkipToast()
    {
        Toast toast = Toast.makeText(context, "NextAlarm is already as soon as possible.", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
