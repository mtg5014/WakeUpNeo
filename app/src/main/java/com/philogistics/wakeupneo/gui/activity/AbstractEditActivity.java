package com.philogistics.wakeupneo.gui.activity;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.gui.widget.AlarmWidget;

import java.util.Calendar;

/**
 * abstract class for alarm create and change screens
 */
public abstract class AbstractEditActivity extends AppCompatActivity {

    protected NextAlarmManager manager;
    public final NextAlarm nextAlarm = new NextAlarm();
    protected Calendar calendar;
    protected TimePicker timePicker = null;
    protected Spinner snoozeSpinner = null;
    protected Button ringtoneButton = null;
    protected Button saveButton = null;
    protected Button cancelButton = null;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = new DefaultNextAlarmManager(getApplicationContext());
        setContentView(getContentView());
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        ringtoneButton = (Button) findViewById(R.id.ringtoneButton);
        snoozeSpinner = (Spinner)findViewById(R.id.snoozeSpinner);
        saveButton = (Button) findViewById (R.id.saveButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        doCreate();
    }

    @Override
    protected final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = null;
        try
        {
            uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
        }
        catch (Exception e)
        {
            //gulp
            e.printStackTrace();
        }
        if(uri != null)
        {
            Ringtone selectedRingtone = RingtoneManager.getRingtone(this,uri);
            nextAlarm.setRingtone(uri.toString());
            Button ringtoneButton = (Button) findViewById(R.id.ringtoneButton);
            ringtoneButton.setText(selectedRingtone.getTitle(this));
        }
    }

    @Override
    protected final void onPause() {
        super.onPause();
        Intent intent = new Intent(this, AlarmWidget.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), AlarmWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(intent);
    }

    protected void doCreate()
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.snoozeValues, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        snoozeSpinner.setAdapter(adapter);
        snoozeSpinner.setSelection(getSpinnerSelection());

        final Uri ringtoneUri = getRingtoneUri();
        nextAlarm.setRingtone(ringtoneUri.toString());
        ringtoneButton.setText(RingtoneManager.getRingtone(this,ringtoneUri).getTitle(this));
        ringtoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select ringtone:");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,RingtoneManager.TYPE_ALARM);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,ringtoneUri);
                startActivityForResult(intent, 999);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int snoozeMinutes = snoozeSpinner.getSelectedItemPosition() < 0 ?
                        Constants.DEFAULT_SNOOZE_LENGTH : snoozeSpinner.getSelectedItemPosition() + 1;
                nextAlarm.setSnoozeOffset(snoozeMinutes);
                nextAlarm.setRinging(false);

                prepareSave(nextAlarm);

                manager.save(nextAlarm);

                finish();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    protected abstract @LayoutRes int getContentView();
    protected abstract void prepareSave(NextAlarm nextAlarm);
    protected abstract int getSpinnerSelection();
    protected abstract Uri getRingtoneUri();

}
