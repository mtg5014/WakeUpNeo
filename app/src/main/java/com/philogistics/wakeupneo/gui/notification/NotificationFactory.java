package com.philogistics.wakeupneo.gui.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.res.ResourcesCompat;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;
import com.philogistics.wakeupneo.listener.service.NotificationService;

/**
 * Created by mtg5014 on 6/10/2016.
 * builds up two types of notifcations
 */
public class NotificationFactory {
    /**
     * generates the notifcation for an alarm
     * @param context
     * @param packageName
     * @param nextAlarm
     * @return
     */
    public static NotificationCompat.Builder NextAlarmNotification(Context context, String packageName, NextAlarm nextAlarm)
    {
        NotificationCompat.Builder alarmNotificationBuilder = new NotificationCompat.Builder(
                context);


        Intent dismissIntent = new Intent(context,NotificationService.class);
        dismissIntent.putExtra(Constants.TIME_IN_MILLIS, nextAlarm.getTimeInMillis());
        dismissIntent.setAction("DISMISS");
        PendingIntent dismissPendingIntent = PendingIntent.getService(context,
                NextAlarmStaticUtilities.getUniqueIntForMillis(nextAlarm.getTimeInMillis()),dismissIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent snoozeIntent = new Intent(context,NotificationService.class);
        snoozeIntent.putExtra(Constants.TIME_IN_MILLIS, nextAlarm.getTimeInMillis());
        snoozeIntent.setAction("SNOOZE");
        PendingIntent snoozePendingIntent = PendingIntent.getService(context,
                NextAlarmStaticUtilities.getUniqueIntForMillis(nextAlarm.getTimeInMillis()),snoozeIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        alarmNotificationBuilder.addAction(R.drawable.ic_snooze_black_24dp,"Snooze",snoozePendingIntent);
        alarmNotificationBuilder.addAction(R.drawable.ic_alarm_off_black_24dp,"Dismiss",dismissPendingIntent);

        alarmNotificationBuilder.setColor(ResourcesCompat.getColor(context.getResources(),R.color.colorAccent,null));
        alarmNotificationBuilder.setSmallIcon(R.drawable.ic_update_black_24dp);

        alarmNotificationBuilder.setCategory(NotificationCompat.CATEGORY_ALARM);
        alarmNotificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        alarmNotificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        alarmNotificationBuilder.setShowWhen(true);

        NextAlarm currentTimeAlarm = new NextAlarm();
        currentTimeAlarm.setTimeInMillis(System.currentTimeMillis());

        alarmNotificationBuilder.setContentTitle(NextAlarmStaticUtilities.getAlarmTime(currentTimeAlarm));
        alarmNotificationBuilder.setContentText(NextAlarmStaticUtilities.getAlarmSnooze(nextAlarm,true));

        Bitmap bitmap = Bitmap.createBitmap(320,320, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(context.getResources().getColor(R.color.colorPrimaryDark,null));
        alarmNotificationBuilder.extend(new NotificationCompat.WearableExtender().setBackground(bitmap));

        return alarmNotificationBuilder;
    }

    /**
     * generates the notification for snoozing an alarm
     * @param context
     * @param packageName
     * @param nextAlarm
     * @return
     */
    public static NotificationCompat.Builder NextAlarmSnoozeNotification(Context context, String packageName, NextAlarm nextAlarm)
    {
        NotificationCompat.Builder alarmNotificationBuilder = new NotificationCompat.Builder(
                context);

        Intent dismissIntent = new Intent(context,NotificationService.class);
        dismissIntent.putExtra(Constants.TIME_IN_MILLIS, nextAlarm.getTimeInMillis());
        dismissIntent.setAction("DISMISS");
        PendingIntent dismissPendingIntent = PendingIntent.getService(context,
                NextAlarmStaticUtilities.getUniqueIntForMillis(nextAlarm.getTimeInMillis()),dismissIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        alarmNotificationBuilder.addAction(R.drawable.ic_snooze_black_24dp,"Dismiss",dismissPendingIntent);

        alarmNotificationBuilder.setColor(ResourcesCompat.getColor(context.getResources(),R.color.colorAccent,null));
        alarmNotificationBuilder.setSmallIcon(R.drawable.ic_update_black_24dp);

        alarmNotificationBuilder.setCategory(NotificationCompat.CATEGORY_ALARM);
        alarmNotificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        alarmNotificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        alarmNotificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
        alarmNotificationBuilder.setShowWhen(true);

        alarmNotificationBuilder.setContentTitle("Snoozing since " + NextAlarmStaticUtilities.getAlarmTime(nextAlarm));

        //just to show when the snoozed alarm will sound
        NextAlarm dummyAlarm = new NextAlarm();
        dummyAlarm.setTimeInMillis(System.currentTimeMillis() + (Constants.MINUTE_IN_MILLIS * nextAlarm.getSnoozeOffset()));
        alarmNotificationBuilder.setContentText("Goes off at " + NextAlarmStaticUtilities.getAlarmTime(dummyAlarm));

        Bitmap bitmap = Bitmap.createBitmap(320,320, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(context.getResources().getColor(R.color.colorPrimaryDark,null));
        alarmNotificationBuilder.extend(new NotificationCompat.WearableExtender().setBackground(bitmap));

        return alarmNotificationBuilder;
    }
}
