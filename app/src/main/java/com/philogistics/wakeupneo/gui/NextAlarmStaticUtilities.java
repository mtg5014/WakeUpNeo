package com.philogistics.wakeupneo.gui;

import com.philogistics.wakeupneo.database.bean.NextAlarm;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mtg5014 on 12/27/16.
 * static utilities for alarms and calendars
 */

public class NextAlarmStaticUtilities
{
    public static Calendar getAlarmCalendar(int hour, int minute)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,hour);
        calendar.set(Calendar.MINUTE,minute);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar;
    }

    public static void updateAlarmCalendar(Calendar calendar, int hour, int minute)
    {
        calendar.set(Calendar.HOUR_OF_DAY,hour);
        calendar.set(Calendar.MINUTE,minute);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
    }

    public static Calendar getAlarmCalendar(long timeInMillis)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        return calendar;
    }

    public static String getAlarmTime(NextAlarm alarm)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(alarm.getTimeInMillis());
        return new StringBuilder()
                .append(calendar.get(Calendar.HOUR) == 0 ? "12" : calendar.get(Calendar.HOUR))
                .append(":")
                .append(String.format("%02d",calendar.get(Calendar.MINUTE)))
                .append(" ")
                .append(calendar.get(Calendar.AM_PM) == 1 ? "PM" : "AM").toString();
    }

    public static String getAlarmDay(NextAlarm alarm)
    {
        Calendar nowCalendar = Calendar.getInstance();
        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.setTimeInMillis(alarm.getTimeInMillis());
        if(alarmCalendar.get(Calendar.DAY_OF_YEAR) == nowCalendar.get(Calendar.DAY_OF_YEAR))
        {
            return "Today";
        }
        else if((alarmCalendar.get(Calendar.DAY_OF_YEAR) - 1) == nowCalendar.get(Calendar.DAY_OF_YEAR))
        {
            return "Tomorrow";
        }
        else
        {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd");
            return formatter.format(alarmCalendar.getTime());
        }
    }

    public static String getAlarmSnooze(NextAlarm alarm, boolean includePrefix)
    {
        String minutes = alarm.getSnoozeOffset() == 1 ? " minute" : " minutes";
        String prefix = includePrefix ? "Snoozes for " : "";
        return prefix + alarm.getSnoozeOffset() + minutes;
    }

    public static void copyNextAlarm(NextAlarm source, NextAlarm dest)
    {
        dest.setId(source.getId());
        dest.setActive(source.isActive());
        dest.setRingtone(source.getRingtone());
        dest.setSnoozeOffset(source.getSnoozeOffset());
        dest.setTimeInMillis(source.getTimeInMillis());
        dest.setRinging(source.isRinging());
    }

    /**
     * we have just enough room in the int atomic type for YYMMDDHHMM
     * @param timeInMillis
     * @return
     */
    public static int getUniqueIntForMillis(long timeInMillis)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmm");
        return Integer.parseInt(formatter.format(calendar.getTime()));
    }

}
