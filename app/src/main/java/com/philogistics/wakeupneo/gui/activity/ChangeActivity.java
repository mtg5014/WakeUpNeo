package com.philogistics.wakeupneo.gui.activity;

import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;

import java.util.Calendar;

/**
 * editing an alarm
 */
public class ChangeActivity extends AbstractEditActivity {

    private long alarmTimeInMillis = -1;

    @Override
    protected void doCreate()
    {
        Intent intent = getIntent();
        alarmTimeInMillis = intent.getLongExtra(Constants.TIME_IN_MILLIS,-1);
        NextAlarm selectedNextAlarm = manager.findByTimeInMillis(alarmTimeInMillis);

        if(selectedNextAlarm == null)
        {
            finish();
        }
        else
        {
            NextAlarmStaticUtilities.copyNextAlarm(selectedNextAlarm,nextAlarm);
            calendar = NextAlarmStaticUtilities.getAlarmCalendar(nextAlarm.getTimeInMillis());
            timePicker.setHour(calendar.get(Calendar.HOUR_OF_DAY));
            timePicker.setMinute(calendar.get(Calendar.MINUTE));

            Button deleteButton = (Button) findViewById(R.id.deleteButton);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    manager.delete(nextAlarm);

                    finish();
                }
            });

            super.doCreate();
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_change;
    }

    @Override
    protected void prepareSave(NextAlarm nextAlarm) {
        NextAlarmStaticUtilities.updateAlarmCalendar(calendar,timePicker.getHour(),
                timePicker.getMinute());

        nextAlarm.setTimeInMillis(calendar.getTimeInMillis());
    }

    @Override
    protected int getSpinnerSelection() {
        return nextAlarm.getSnoozeOffset() - 1;
    }

    @Override
    protected Uri getRingtoneUri() {
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        try
        {
            uri = Uri.parse(nextAlarm.getRingtone());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return uri;
    }
}
