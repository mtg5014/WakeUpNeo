package com.philogistics.wakeupneo.gui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;

/**
 * activity that fires when an alarm goes off
 */
public class AlarmActivity extends AppCompatActivity {

    private NextAlarm nextAlarm;
    private NextAlarmManager manager;
    private BroadcastReceiver broadcast_reciever;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        this.setVolumeControlStream(AudioManager.STREAM_ALARM);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alarm);

        manager = new DefaultNextAlarmManager(this);

        broadcast_reciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
                    finish();
                }
            }
        };
        registerReceiver(broadcast_reciever, new IntentFilter("finish_activity"));

        TextView alarmTime = (TextView) findViewById(R.id.alarmTime);
        NextAlarm nowAlarm = new NextAlarm();
        nowAlarm.setTimeInMillis(System.currentTimeMillis());
        alarmTime.setText(NextAlarmStaticUtilities.getAlarmTime(nowAlarm));

        long alarmTimeInMillis = getIntent().getLongExtra(Constants.TIME_IN_MILLIS,-1);
        nextAlarm = manager.findByTimeInMillis(alarmTimeInMillis);
        if(nextAlarm != null) {
            TextView snoozeTime = (TextView) findViewById(R.id.snoozeTime);
            snoozeTime.setText(NextAlarmStaticUtilities.getAlarmSnooze(nextAlarm, false));
        }

        Button dismissButton = (Button) findViewById(R.id.dismissButton);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nextAlarm == null) {
                    long alarmTimeInMillis = getIntent().getLongExtra(Constants.TIME_IN_MILLIS, -1);
                    nextAlarm = manager.findByTimeInMillis(alarmTimeInMillis);
                }
                if(nextAlarm != null)
                {
                    manager.dismiss(nextAlarm);
                }

                finish();
            }
        });

        Button snoozeButton = (Button) findViewById(R.id.snoozeButton);
        snoozeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                snooze();
                finish();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
                || keyCode == KeyEvent.KEYCODE_VOLUME_UP
                || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE)
        {
            snooze();
            finish();
        }
        return super.onKeyUp(keyCode,event);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    public void finish()
    {
        unregisterReceiver(broadcast_reciever);
        super.finish();
    }

    private void snooze() {
        if(nextAlarm == null) {
            long timeInMillis = this.getIntent().getLongExtra(Constants.TIME_IN_MILLIS, -1);
            NextAlarm nextAlarm = manager.findByTimeInMillis(timeInMillis);
        }
        if (nextAlarm != null) {
            manager.snooze(nextAlarm);
        }
    }

    @Override
    public void onBackPressed() {
        //do nothing
    }

    @Override
    protected void onUserLeaveHint() {
        //if they exit for whatever reason, snooze the alarm
        snooze();
        finish();
        super.onUserLeaveHint();
    }
}