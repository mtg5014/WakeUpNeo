package com.philogistics.wakeupneo.gui.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Pair;
import android.view.Gravity;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.gui.activity.MainActivity;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.listener.receiver.WidgetUpdateReceiver;

import java.util.concurrent.TimeUnit;

/**
 * Implementation of App Widget functionality.
 */
public class AlarmWidget extends AppWidgetProvider {

    public static String NEXT_ACTION = "NextAction";
    public static String PREV_ACTION = "PrevAction";
    public static String OPEN_ACTION = "OpenAction";
    public static String VOLUME_ACTION = "VolumeAction";

    private static NextAlarm nextAlarm;
    private static AlarmManager alarmManager;
    private NextAlarmManager nextAlarmManager;

    public void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        if(nextAlarmManager == null) {
            nextAlarmManager = new DefaultNextAlarmManager(context.getApplicationContext());
        }

        CharSequence widgetText;
        CharSequence dayText = null;
        nextAlarm = nextAlarmManager.findNextActive();
        if (nextAlarm == null) {
            widgetText = context.getString(R.string.no_alarms);
            dayText = context.getString(R.string.app_name);
        } else {
            widgetText = NextAlarmStaticUtilities.getAlarmTime(nextAlarm);
            dayText = NextAlarmStaticUtilities.getAlarmDay(nextAlarm);
        }

        Pair<Integer, CharSequence> remainingPair = getTimeRemaining(nextAlarm);

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.alarm_widget);
        views.setTextViewText(R.id.appwidget_text, widgetText);
        views.setTextViewText(R.id.titleText,dayText);
        //views.setTextColor(R.id.appwidget_text, context.getResources().getColor(statusColor));
        //views.setTextViewText(R.id.hourText, remainingPair.second);
        views.setProgressBar(R.id.progressBar, 12*60, remainingPair.first, false);

        Intent nextIntent = new Intent(context, AlarmWidget.class);
        nextIntent.setAction(NEXT_ACTION);
        PendingIntent nextPendingIntent = PendingIntent.getBroadcast(context, 0, nextIntent, 0);

        Intent prevIntent = new Intent(context, AlarmWidget.class);
        prevIntent.setAction(PREV_ACTION);
        PendingIntent prevPendingIntent = PendingIntent.getBroadcast(context, 0, prevIntent, 0);

        Intent openIntent = new Intent(context, AlarmWidget.class);
        openIntent.setAction(OPEN_ACTION);
        PendingIntent openPendingIntent = PendingIntent.getBroadcast(context, 0, openIntent,
                0);

        views.setOnClickPendingIntent(R.id.nextButton, nextPendingIntent);
        views.setOnClickPendingIntent(R.id.prevButton, prevPendingIntent);
        views.setOnClickPendingIntent(R.id.appwidget_text, nextPendingIntent);
        views.setOnClickPendingIntent(R.id.titleText,prevPendingIntent);
        views.setOnClickPendingIntent(R.id.progressBar, openPendingIntent);
        views.setOnClickPendingIntent(R.id.mainButton, openPendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        alarmManager =  (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        Intent intent = new Intent(context, WidgetUpdateReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC, 0,60000,pendingIntent);
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        alarmManager =  (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        Intent intent = new Intent(context, WidgetUpdateReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (action == NEXT_ACTION || action == PREV_ACTION) {
            if(nextAlarmManager == null) {
                nextAlarmManager = new DefaultNextAlarmManager(context.getApplicationContext());
            }
            NextAlarm dbNextAlarm = nextAlarmManager.findNextActive();
            long dbAlarmTimeInMillis = -1;
            long alarmTimeInMillis = -1;
            if (dbNextAlarm != null) {
                dbAlarmTimeInMillis = dbNextAlarm.getTimeInMillis();
            }
            if (nextAlarm != null) {
                alarmTimeInMillis = nextAlarm.getTimeInMillis();
            }
            //if we aren't up to date, just fall out and let the refresh happen
            if (dbAlarmTimeInMillis == alarmTimeInMillis) {
                if (intent.getAction() == NEXT_ACTION) {
                    doNext(context);
                } else if (intent.getAction() == PREV_ACTION) {
                    doPrevious(context);
                }
            }
        }
        if (action == OPEN_ACTION) {
            Intent newIntent = new Intent(context, MainActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newIntent);
        }
        if (action == AppWidgetManager.ACTION_APPWIDGET_UPDATE || action == PREV_ACTION || action == NEXT_ACTION) {
            //and reupdate the widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), AlarmWidget.class.getName());
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
            onUpdate(context, appWidgetManager, appWidgetIds);
        }
    }


    private Pair<Integer, CharSequence> getTimeRemaining(NextAlarm nextAlarm) {
        CharSequence timeRemaining;
        long delta = 0;
        long totalMinutesLeft = 0;
        int hoursLeft = 0;
        long minutesLeft = 0;
        if (nextAlarm != null) {
            delta = nextAlarm.getTimeInMillis() - System.currentTimeMillis();

            totalMinutesLeft = TimeUnit.MILLISECONDS.toMinutes(delta);
            minutesLeft = totalMinutesLeft % 60;
            hoursLeft = (int) totalMinutesLeft / 60;

            if(hoursLeft < 24) {
                String hoursLeftString = String.format("%02d", hoursLeft);
                String minutesLeftString = String.format("%02d", minutesLeft);

                timeRemaining = hoursLeftString + ":" + minutesLeftString;
            }
            else
            {
                timeRemaining = "";
            }
        } else {
            timeRemaining = "---";
        }
        return new Pair<Integer, CharSequence>((int)((hoursLeft*60) + minutesLeft), timeRemaining);
    }

    private int setVolumeImage(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        double volPercent = (double) currentVolume / (double) maxVolume;
        if (volPercent > .75) {
            return R.drawable.ic_volume_up_green_800_24dp;
        } else if (volPercent > .5) {
            return R.drawable.ic_volume_down_yellow_a700_24dp;
        } else {
            return R.drawable.ic_volume_off_red_800_18dp;
        }
    }

    private void doNext(Context context)
    {
        if(nextAlarm == null)
        {
            Toast toast = Toast.makeText(context,"All alarms are off.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
        }
        else {
            try {
                nextAlarmManager.skip(nextAlarm, true,Constants.DAY_IN_MILLIS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void doPrevious(Context context)
    {
        if (nextAlarm == null) {
            Toast toast = Toast.makeText(context,"All alarms are off.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
        }
        else
        {
            try {
                nextAlarmManager.skip(nextAlarm,false,Constants.DAY_IN_MILLIS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

