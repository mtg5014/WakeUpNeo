package com.philogistics.wakeupneo.gui.array;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;
import com.philogistics.wakeupneo.gui.activity.ChangeActivity;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;

import java.util.List;

/**
 * Created by mtg5014 on 5/19/2016.
 * adapter for the list of alarms
 */
public class AlarmArrayAdapter extends ArrayAdapter<NextAlarm> {
    int resource;
    String response;
    Context context;
    private NextAlarmManager manager;


    public AlarmArrayAdapter(Context context, int resource, List<NextAlarm> nextAlarms)
    {
        super(context, resource, nextAlarms);
        this.context = context;
        this.resource = resource;
        this.manager = new DefaultNextAlarmManager(context.getApplicationContext());
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent)
    {
        LinearLayout alarmView;
        final NextAlarm nextAlarm = getItem(position);

        if(convertView==null)
        {
            alarmView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater)getContext().getSystemService(inflater);
            vi.inflate(resource,alarmView,true);
        }
        else
        {
            alarmView = (LinearLayout) convertView;
        }

        TextView alarmText = (TextView) alarmView.findViewById(R.id.listAlarmText);
        alarmText.setText(NextAlarmStaticUtilities.getAlarmTime(nextAlarm) + System.getProperty("line.separator") + NextAlarmStaticUtilities.getAlarmDay(nextAlarm));

        alarmText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeActivity.class);
                intent.putExtra(Constants.TIME_IN_MILLIS, nextAlarm.getTimeInMillis());
                getContext().startActivity(intent);
            }
        });

        Switch alarmSwitch = (Switch) alarmView.findViewById(R.id.listActiveSwitch);
        //make this null first to prevent the mysterious bogus update
        alarmSwitch.setOnCheckedChangeListener(null);
        alarmSwitch.setChecked(nextAlarm.isActive() ? true : false);

        alarmSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    nextAlarm.setActive(isChecked);

                    manager.save(nextAlarm);
                    TextView dayText = (TextView) ((ViewGroup) buttonView.getParent()).findViewById(R.id.listAlarmText);
                    dayText.setText(NextAlarmStaticUtilities.getAlarmTime(nextAlarm) + System.getProperty("line.separator") + NextAlarmStaticUtilities.getAlarmDay(nextAlarm));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ImageView prevButton = (ImageView) alarmView.findViewById(R.id.prevButton);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    manager.skip(nextAlarm,false, Constants.DAY_IN_MILLIS);
                    TextView dayText = (TextView) ((ViewGroup) v.getParent()).findViewById(R.id.listAlarmText);
                    dayText.setText(NextAlarmStaticUtilities.getAlarmTime(nextAlarm) + System.getProperty("line.separator") + NextAlarmStaticUtilities.getAlarmDay(nextAlarm));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ImageView nextButton = (ImageView) alarmView.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    manager.skip(nextAlarm,true,Constants.DAY_IN_MILLIS);
                    TextView dayText = (TextView) ((ViewGroup) v.getParent()).findViewById(R.id.listAlarmText);
                    dayText.setText(NextAlarmStaticUtilities.getAlarmTime(nextAlarm) + System.getProperty("line.separator") + NextAlarmStaticUtilities.getAlarmDay(nextAlarm));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return alarmView;
    }

}
