package com.philogistics.wakeupneo.gui.activity;

import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.LayoutRes;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;

/**
 * adding an alarm
 */
public class AddActivity extends AbstractEditActivity {

    private NextAlarmManager manager;
    private final NextAlarm nextAlarm = new NextAlarm();

    @Override
    protected @LayoutRes int getContentView() {
        return R.layout.activity_add;
    }

    @Override
    protected void prepareSave(NextAlarm nextAlarm) {
        calendar = NextAlarmStaticUtilities.getAlarmCalendar(timePicker.getHour(),
                timePicker.getMinute());;
        nextAlarm.setTimeInMillis(calendar.getTimeInMillis());
        nextAlarm.setActive(true);
    }

    @Override
    protected int getSpinnerSelection() {
        return Constants.DEFAULT_SNOOZE_LENGTH - 1;
    }

    @Override
    protected Uri getRingtoneUri() {
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    }
}
