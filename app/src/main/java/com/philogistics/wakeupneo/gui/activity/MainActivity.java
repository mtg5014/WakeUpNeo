package com.philogistics.wakeupneo.gui.activity;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;
import com.philogistics.wakeupneo.R;
import com.philogistics.wakeupneo.gui.array.AlarmArrayAdapter;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.gui.widget.AlarmWidget;

import java.util.List;

/**
 * main list of alarms
 */
public class MainActivity extends AppCompatActivity {

    List<NextAlarm> nextAlarms;
    AlarmArrayAdapter adapter;
    private NextAlarmManager manager;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,AddActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume()
    {
        manager = new DefaultNextAlarmManager(getApplicationContext());
        nextAlarms = manager.findAll(NextAlarm.class);
        adapter = new AlarmArrayAdapter(this,
                R.layout.alarm_list_item, nextAlarms);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent intent = new Intent(this, AlarmWidget.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), AlarmWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(intent);
    }
}
