package com.philogistics.wakeupneo.listener.service;

import android.app.IntentService;
import android.content.Intent;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;

/**
 * Created by mtg5014 on 6/3/2016.
 * handles notification actions
 */
public class NotificationService extends IntentService {

    private NextAlarm nextAlarm;
    private NextAlarmManager manager;

    public NotificationService(){
        super("NotificationService");
    }

    @Override
    public void onHandleIntent(Intent intent) {
        manager = new DefaultNextAlarmManager(getApplicationContext());
        if(intent.getAction().equals("DISMISS")){
            dismiss(intent);
        }
        else if(intent.getAction().equals("SNOOZE"))
        {
            snooze(intent);
        }
    }

    private void dismiss(Intent intent)
    {
        long timeInMillis = intent.getLongExtra(Constants.TIME_IN_MILLIS,-1);
        nextAlarm = manager.findByTimeInMillis(timeInMillis);
        if(nextAlarm != null) {
            manager.dismiss(nextAlarm);
        }
    }

    private void snooze(Intent intent)
    {
        long timeInMillis = intent.getLongExtra(Constants.TIME_IN_MILLIS,-1);
        nextAlarm = manager.findByTimeInMillis(timeInMillis);
        if(nextAlarm != null) {
            manager.snooze(nextAlarm);
        }
    }
}
