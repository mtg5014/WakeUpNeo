package com.philogistics.wakeupneo.listener.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;
import com.philogistics.wakeupneo.gui.NextAlarmStaticUtilities;
import com.philogistics.wakeupneo.gui.activity.AlarmActivity;
import com.philogistics.wakeupneo.gui.notification.NotificationFactory;

/**
 * Created by mtg5014 on 5/21/2016.
 * responds to an alarm by actually setting things off (sounds, notifications, screens)
 */
public class AlarmService extends IntentService{
    private NotificationManager notificationManager;
    private NextAlarmManager manager;

    public AlarmService(){
        super("AlarmService");
    }

    @Override
    public void onHandleIntent(Intent intent) {

        manager = new DefaultNextAlarmManager(getApplicationContext());
        long alarmTimeInMillis = intent.getLongExtra(Constants.TIME_IN_MILLIS,-1);
        NextAlarm nextAlarm = null;

        if(alarmTimeInMillis > -1)
        {
            nextAlarm = manager.findByTimeInMillis(alarmTimeInMillis);
        }
        String msg = "NextAlarm";
        if(nextAlarm != null) {
            if(manager.collisionExists(nextAlarm))
            {
                manager.deferCollission(nextAlarm,Constants.MINUTE_IN_MILLIS/10);
            }
            else
            {
                //in this VERY special case, we use NextAlarm direclty to save the fact that the alarm is NOT ringing
                //the ringing field is systematic only and we want none of the side effects from he manager save method,
                //only to save the bean
                nextAlarm.setRinging(true);
                NextAlarm.save(nextAlarm);

                msg = NextAlarmStaticUtilities.getAlarmTime(nextAlarm);

                Intent alarmIntent = new Intent(this, AlarmActivity.class);
                alarmIntent.putExtra(Constants.TIME_IN_MILLIS, alarmTimeInMillis);
                alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(alarmIntent);

                Log.d("AlarmService", "Preparing to send notification...: " + msg);
                notificationManager = (NotificationManager) this
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationCompat.Builder alarmNotificationBuilder = NotificationFactory.NextAlarmNotification(this, getPackageName(), nextAlarm);

                notificationManager.notify(NextAlarmStaticUtilities.getUniqueIntForMillis(alarmTimeInMillis), alarmNotificationBuilder.build());
                Log.d("AlarmService", "Notification sent.");

                //now fire the media
                Intent mediaIntent = new Intent(this, MediaService.class);
                mediaIntent.putExtra(Constants.TIME_IN_MILLIS, alarmTimeInMillis);
                this.startService(mediaIntent);
            }
        }
    }
}
