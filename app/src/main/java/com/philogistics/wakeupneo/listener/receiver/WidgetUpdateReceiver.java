package com.philogistics.wakeupneo.listener.receiver;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.philogistics.wakeupneo.gui.widget.AlarmWidget;

/**
 * Created by mtg5014 on 5/28/2016.
 * updates the widget
 */
public class WidgetUpdateReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent newIntent = new Intent(context, AlarmWidget.class);
        newIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        ComponentName widget = new ComponentName(context.getApplicationContext(),AlarmWidget.class);
        int ids[] = AppWidgetManager.getInstance(context.getApplicationContext()).getAppWidgetIds(widget);
        newIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        context.getApplicationContext().sendBroadcast(newIntent);
    }
}
