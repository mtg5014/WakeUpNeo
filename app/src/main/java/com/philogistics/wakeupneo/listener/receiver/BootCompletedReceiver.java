package com.philogistics.wakeupneo.listener.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;

import java.util.List;

/**
 * handles setting alarms when we reboot the phone
 */
public class BootCompletedReceiver extends BroadcastReceiver {
    public BootCompletedReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NextAlarmManager manager = new DefaultNextAlarmManager(context.getApplicationContext());
        List<NextAlarm> activeNextAlarms = manager.findByActive(true);
        for(NextAlarm nextAlarm : activeNextAlarms)
        {
            manager.save(nextAlarm);
        }
    }
}
