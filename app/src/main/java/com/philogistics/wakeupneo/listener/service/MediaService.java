package com.philogistics.wakeupneo.listener.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;

import com.philogistics.wakeupneo.Constants;
import com.philogistics.wakeupneo.database.bean.NextAlarm;
import com.philogistics.wakeupneo.database.manager.nextAlarm.DefaultNextAlarmManager;
import com.philogistics.wakeupneo.database.manager.nextAlarm.NextAlarmManager;

/**
 * plays sounds and vibrates when an alarm goes off
 */
public class MediaService extends Service {
    private MediaPlayer mp;
    private Vibrator v;
    private AudioManager am;
    private NextAlarmManager manager;

    public MediaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        mp = new MediaPlayer();
        AudioManager am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        am.setStreamVolume(AudioManager.STREAM_ALARM,am.getStreamMaxVolume(AudioManager.STREAM_ALARM),0);
        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        manager = new DefaultNextAlarmManager(this);
        long alarmTimeInMillis = intent.getLongExtra(Constants.TIME_IN_MILLIS,-1);
        NextAlarm nextAlarm = manager.findByTimeInMillis(alarmTimeInMillis);
        if(nextAlarm != null && nextAlarm.getRingtone() != null)
        {
            alarmUri = Uri.parse(nextAlarm.getRingtone());
        }

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0,100,100};
        v.vibrate(pattern,0);
        try {
            mp.setDataSource(getApplicationContext(), alarmUri);
            mp.setAudioStreamType(AudioManager.STREAM_ALARM);
            mp.setLooping(true);
            mp.prepare();
            mp.start();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        mp.stop();
        v.cancel();
    }
}
